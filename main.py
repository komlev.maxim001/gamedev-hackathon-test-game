import pygame
import random
import objects
screen_size = [1280, 720]
game_size = screen_size
print("Hello!")
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("Snake")
clock = pygame.time.Clock()
menu = pygame.Surface(screen_size)
menu_pic = objects.Menu(screen_size)
platform = pygame.surface.Surface((1280, 60))
platform.fill((152, 251, 152))
snake_block = 20
snake_speed = 15
font_style = pygame.font.Font('19363.ttf', 35)
score_font = pygame.font.Font('19363.ttf', 30)

time = 0
def Your_score(score):
    global time
    value = score_font.render("Your Score: " + str(score) + "       " + "time: " + str(time), True, (0, 0, 0))
    platform.fill((152, 251, 152))
    pygame.display.set_caption("Snake [Score: " + str(score) + "]")
    platform.blit(value, [10, 5])


def our_snake(snake_block, snake_list):
    for x in snake_list:
        pygame.draw.rect(screen, (47, 79, 79), [x[0], x[1], snake_block, snake_block])


def message(msg, color, spacefill: list): #Выводит сообщения, которые нам надо
    mesg = font_style.render(msg, True, color)
    screen.blit(mesg, [screen_size[0] // 3 + spacefill[0], screen_size[1] // 2 + spacefill[1]])

def gameLoop(): #Геймплей змейки
    game_over = False
    global time
    frames = 0
    game_close = False
    game_pause = False
    game_menu = True
    keys = {}
    x1 = screen_size[0] / 2
    y1 = screen_size[1] / 2

    x1_change = 0
    y1_change = 0
    snake_List = []
    Length_of_snake = 1

    food_x = round(random.randrange(0, screen_size[0] - snake_block) / 20.0) * 20.0 #Создает координаты для еды
    food_y = round(random.randrange(0, screen_size[1] - snake_block) / 20.0) * 20.0
    if food_y < 60:
        food_y += 60
    while not game_over:
        clock.tick(15)
        if game_menu:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                            game_menu = not game_menu
                            game_over = False
                            game_close = False

                    keys[event.key] = True

                if event.type == pygame.KEYUP:
                    keys[event.key] = False
            menu.fill((0, 255, 127))
            menu.blit(font_style.render(("Чтобы начать игру нажмите [Пробел]"), True, (255, 165, 0)), [100, screen_size[1] // 2])
            menu.blit(font_style.render(("(Управление на стрелочках)"), True, (255, 165, 0)), [100, screen_size[1] // 1.2])
            menu_pic.update()
            menu.blit(menu_pic.image, menu_pic.position)
            screen.blit(menu, menu.get_rect())
        elif game_pause:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        game_pause = False
            message(("Game Paused"), (0,0,0), [550, -363])
        else:
            game_over_message = False
            while game_close == True:
                screen.fill((34, 139, 34))
                if game_over_message == False:
                    message(("Вы проиграли со счетом: " + str(int(Length_of_snake - 1))), (255, 165, 0), [-80, -100])
                    message("Для выхода в меню нажмите [esc]", (255, 165, 0), [-150,0])
                    pygame.display.set_caption("Snake")
                    game_over_message = True
                    pygame.display.update()

                for event in pygame.event.get():
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_ESCAPE	:
                            gameLoop()
                            game_menu = not game_menu
                            game_pause = False
                            game_close = False
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    game_over = True
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        x1_change = -snake_block
                        y1_change = 0
                    elif event.key == pygame.K_ESCAPE:
                        game_pause = not game_pause
                    elif event.key == pygame.K_RIGHT:
                        x1_change = snake_block
                        y1_change = 0
                    elif event.key == pygame.K_UP:
                        y1_change = -snake_block
                        x1_change = 0
                    elif event.key == pygame.K_DOWN:
                        y1_change = snake_block
                        x1_change = 0
            if x1 >= screen_size[0] or x1 < 0 or y1 >= screen_size[1] or y1 < 60:
                game_close = True
            x1 += x1_change
            y1 += y1_change
            screen.fill((50, 205, 50))
            pygame.draw.rect(screen, (165, 42, 42), [food_x, food_y, snake_block, snake_block])
            snake_Head = []
            if frames == 15:
                time += 1
                frames = 0
            frames += 1

            snake_Head.append(x1)
            snake_Head.append(y1)
            snake_List.append(snake_Head)
            if len(snake_List) > Length_of_snake:
                del snake_List[0]

            for x in snake_List[:-1]:
                if x == snake_Head:
                    game_close = True

            our_snake(snake_block, snake_List)

            if x1 == food_x and y1 == food_y:
                food_x = round(random.randrange(0, screen_size[0] - snake_block) / 20.0) * 20.0
                food_y = round(random.randrange(0, screen_size[1] - snake_block) / 20.0) * 20.0
                if food_y < 60:
                    food_y += 60
                Length_of_snake += 1
            Your_score(Length_of_snake - 1)
            screen.blit(platform, (0, 0))
        pygame.display.update()
        pygame.display.flip()
    pygame.quit()
    quit()


gameLoop()
